-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.1.30-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win32
-- HeidiSQL Version:             9.5.0.5253
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Exportiere Datenbank Struktur für gaestebuch
CREATE DATABASE IF NOT EXISTS `gaestebuch` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gaestebuch`;

-- Exportiere Struktur von Tabelle gaestebuch.list
CREATE TABLE IF NOT EXISTS `list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nachricht` text NOT NULL,
  `reply` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_list_list` (`reply`),
  CONSTRAINT `FK_list_list` FOREIGN KEY (`reply`) REFERENCES `list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Exportiere Daten aus Tabelle gaestebuch.list: ~9 rows (ungefähr)
DELETE FROM `list`;
/*!40000 ALTER TABLE `list` DISABLE KEYS */;
INSERT INTO `list` (`id`, `name`, `email`, `nachricht`, `reply`, `created`) VALUES
	(1, 'Sven Nolting', 'contact@rindula.de', 'Hallo Welt', NULL, '2018-07-30 14:11:07'),
	(2, 'Jemand anderes', 'service@rindula.de', 'Hallo Parkuhr', NULL, '2018-07-30 14:11:07'),
	(3, 'Noch jemand', 'info@rindula.de', 'Nope...', 1, '2018-07-30 14:11:07'),
	(4, 'Ich bims', 'blah@blah.de', '<a href="load.php">Hierher</a>', 3, '2018-07-30 14:11:07'),
	(5, 'Sven Nolting', 'service@rindula.de', 'Test', NULL, '2018-07-30 15:06:25'),
	(6, 'Sven Nolting', 'service@rindula.de', 'Test', 4, '2018-07-30 15:06:56'),
	(7, 'Sven Nolting', 'rindola@t-online.de', 'Test', 1, '2018-07-30 15:23:17'),
	(8, 'Sven Nolting', 'service@rindula.de', 'Teeeest', 2, '2018-07-30 15:23:31'),
	(9, 'Sven Nolting', 'service@rindula.de', 'TTTTTEEEESSSSTTTT', 1, '2018-07-30 15:23:56'),
	(10, 'Sven Nolting', 'rindola@t-online.de', 'Testen wirs...', 6, '2018-07-30 16:23:44'),
	(11, 'Sven Nolting', 'rindola@t-online.de', 'Nope', 6, '2018-07-30 16:24:02'),
	(12, 'Sven Nolting', 'rindola@t-online.de', 'TTEEESSSSTTTTT', 6, '2018-07-30 16:26:09');
/*!40000 ALTER TABLE `list` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
