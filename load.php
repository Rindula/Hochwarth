<?php

include_once "config.php";

function searchForId($id, $array) {
    foreach ($array as $key => $val) {
        if ($val['id'] === $id) {
            return $key;
        }
    }
    return null;
 }
$modals = "";
 function makeList($values) {
    global $replyList, $list, $modals;
    $name = $values["name"];
    $email = $values["email"];
    $message = htmlspecialchars($values["message"]);
    $id = $values["id"];

    $timestamp = date("d.m.Y, H:m:i", strtotime($values["timestamp"]));

    echo "<div class='list-group-item'><span><div class='border-bottom d-inline'><p class='border-left pl-2'>$name<br><small>$timestamp</small></p><a class='border-left pl-2' href='mailto:$email'>$email</a></div><div class='d-inline float-right'><a data-toggle='modal' data-target='#answer$id' class='btn btn-primary text-light'>Antworten</a></div></span><span class='p-4'><div class='entry'>$message</div></span>";

    foreach ($replyList as $value) {
        if ($value[0] == $values["id"]) {
            makeList($list[$value[1]]);
        }
    }
    
    echo "</div>";

    $modals .= '
<div class="modal fade" id="answer'.$id.'" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">'.$name.' antworten</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
        <form id="form'.$values["id"].'" class="form" action="enter.php" method="post">
            <input type="hidden" name="reply" value="'.$values["id"].'">
            <div class="form-group">
                <input class="form-control" autocomplete="name" type="text" name="name" id="name" placeholder="Name">
            </div>
            <div class="form-group">
                <input class="form-control" autocomplete="email" type="text" name="email" id="email" placeholder="E-Mail Adresse">
            </div>
            <div class="form-group">
                <textarea style="resize: none;" class="form-control" name="entry" placeholder="Nachricht..."></textarea>
            </div>
        </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
            <button type="button" class="btn btn-primary" onclick="document.getElementById(\'form'.$values["id"].'\').submit()">Antworten</button>
        </div>
        </div>
    </div>
</div>
    ';
 }

$dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);


foreach ($dbh->query('SELECT id as "id", name as "name", email as "email", nachricht as "message", reply as "reply", created as "timestamp" FROM list') as $row) {
    $list[$row["id"]] = array("id" => $row["id"], "name" => $row["name"], "email" => $row["email"], "message" => $row["message"], "reply" => $row["reply"], "timestamp" => $row["timestamp"]);
}

foreach ($list as $value) {
    if (isset($value["reply"])) {
        $replyList[] = array($value["reply"], $value["id"]);
    }
}

foreach ($list as $value) {
    if (!isset($value["reply"])) makeList($value);
} 
echo $modals;
