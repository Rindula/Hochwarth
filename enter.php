<?php

include_once "config.php";

$error = false;

$name = $_POST["name"];
$email = $_POST["email"];
$nachricht = $_POST["entry"];

$reply = ((isset($_POST["reply"])) ? $_POST["reply"] : null );

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $error = true;
    $returnData = array("success" => false, "message" => "Bitte überprüfe die eingegebene E-Mail Adresse!");
}

if (!$error && empty($name)) {
    $error = true;
    $returnData = array("success" => false, "message" => "Bitte geben Sie ihren Namen ein!");
}

if (!$error && empty($nachricht)) {
    $error = true;
    $returnData = array("success" => false, "message" => "Wenn Sie nicht schreiben wollen, brauchen Sie auch nichts senden. Sie haben keine Nachricht eingegeben!");
}

if (!$error) {
    $dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);

    $sth = $dbh->prepare("INSERT INTO list (name, email, nachricht, reply) VALUES (:name, :email, :msg, :reply)");
    $sth->bindValue(":name", $name);
    $sth->bindValue(":email", $email);
    $sth->bindValue(":msg", $nachricht);
    $sth->bindValue(":reply", $reply);
    $sth->execute();

    $returnData = array("success" => true, "message" => "Der Eintrag wurde erfolgreich gespeichert.");

}

echo json_encode($returnData);

header("Location: .");